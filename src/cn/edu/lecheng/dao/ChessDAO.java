package cn.edu.lecheng.dao;

import java.sql.*;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import cn.edu.lecheng.pojo.Chess;
import cn.edu.lecheng.util.Database;

public class ChessDAO {
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	/**
	 * 为chess插入数据
	 * @param id
	 * @param username
	 * @param trueName
	 * @param sex
	 * @return
	 * 创建者：TOM
	 * 创建日期：2013-8-6
	 */
	public boolean addChess(String clientname,int step,String position, String playtime,int gameid){
		int rut = 0;
		
		String sql = "INSERT INTO Chess(clientname,step,position,playtime,gameid) VALUES(?,?,?,?,?)";
		Database db = new Database();
		conn = db.getConn();
		try{
			ps = conn.prepareStatement(sql);
			ps.setString(1, clientname);
			ps.setInt(2, step);
			ps.setString(3, position);
			ps.setString(4, playtime);
			ps.setInt(5, gameid);
			rut = ps.executeUpdate();
			db.closeConn(conn, ps);
		}catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		if(rut > 0){
			return true;
		}else{
			return false;
		}
	}
	/**
	 * 
	 * 为game表插入数据
	 * @param clientname
	 * @param step
	 * @param position
	 * @param playtime
	 * @return
	 * cuipanpan
	 * 2013-8-7
	 */
	
	public boolean addGame(int id, String starttime,String winner, String endtime){
		int rut = 0;
		
		String sql = "INSERT INTO game(id,starttime,winner,endtime) VALUES(?,?,?,?)";
		Database db = new Database();
		conn = db.getConn();
		try{
			ps.setInt(1, id);
			ps.setString(2, starttime);
			ps.setString(3, winner);
			ps.setString(4, endtime);
			rut = ps.executeUpdate();
			db.closeConn(conn, ps);
		}catch(Exception e){
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		if(rut > 0){
			return true;
		}else{
			return false;
		}
	}
//	/**
//	 * 根据用户名查询订单信息
//	 * @param username
//	 * @return
//	 * 创建者：TOM
//	 * 创建日期：2013-8-6
//	 */
//	public ArrayList<Chess> queryUserOrders(String username){
//		ArrayList<Chess> list = new ArrayList<Chess>();
//		String sql = "SELECT customers.c_Name,customers.c_TrueName,orders.o_ID,orders.o_SendMode,orders.o_Sum"
//				+" FROM customers INNER JOIN orders ON customers.c_ID=orders.c_ID"
//				+" WHERE customers.c_Name = ?";
//		Database db = new Database();
//		conn = db.getConn();
//		try{
//			ps = conn.prepareStatement(sql);
//			ps.setString(1, username);
//			rs = ps.executeQuery();
//			while(rs.next()){
//				Chess cus = new Chess();
//				cus.setId(rs.getInt(1));
//				cus.setClientName(rs.getString(2));
//				cus.setStep(rs.getInt(3));
//				cus.setPosition(rs.getString(4));
//				cus.setPlaytime(rs.getString(5));
//				list.add(cus);
//			}
//			db.closeConn(conn, ps, rs);
//		}catch(Exception e){
//			System.out.println(e.getMessage());
//		}
//		return list;
//	}
//	
//	/**
//	 * 
//	 * 根据用户名修改type
//	 * @param username
//	 * @return
//	 * cuipanpan
//	 * 2013-8-6
//	 */
//	public boolean update(String username){
//		String sql = "UPDATE customers SET customers.c_Type = 'VIP' WHERE customers.c_Name = ?";
//		Database db = new Database();
//		conn = db.getConn();
//		int rut = 0;
//		try{
//			ps = conn.prepareStatement(sql);
//			ps.setString(1, username);
//			rut = ps.executeUpdate();
//			db.closeConn(conn, ps, rs);
//		}catch(Exception e){
//			System.out.println(e.getMessage());
//		}
//		if(rut > 0){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	
//	public boolean delete(String username){
//		String sql = "DELETE FROM customers WHERE customers.c_Name = ?";
//		Database db = new Database();
//		conn = db.getConn();
//		int rut = 0;
//		try{
//			ps = conn.prepareStatement(sql);
//			ps.setString(1, username);
//			rut = ps.executeUpdate();
//			db.closeConn(conn, ps, rs);
//		}catch(Exception e){
//			System.out.println(e.getMessage());
//		}
//		if(rut > 0){
//			return true;
//		}else{
//			return false;
//		}
//	}
	
}
