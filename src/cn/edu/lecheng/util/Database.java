package cn.edu.lecheng.util;

import java.sql.*;

public class Database {
	Connection conn = null;
	PreparedStatement ps = null;
	ResultSet rs = null;
	private String drvStr = "com.mysql.jdbc.Driver";
	private String connStr = "jdbc:mysql://127.0.0.1:3306/playgo?user=root&password=123";
	/**
	 * 连接数据库
	 * @return
	 * 创建者：TOM
	 * 创建日期：2013-8-6
	 */
	public Connection getConn(){
		try{
			Class.forName(drvStr);
			conn = DriverManager.getConnection(connStr);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		return conn;
	}
	/**
	 * 关闭Conn一个参数
	 * @param conn
	 * 创建者：TOM
	 * 创建日期：2013-8-6
	 */
	public void closeConn(Connection myConn){
		conn = myConn;
		if(conn != null){
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	/**
	 * 关闭Conn和ps两个参数，方法的重载
	 * @param myConn
	 * @param myPs
	 * 创建者：TOM
	 * 创建日期：2013-8-6
	 */
	public void closeConn(Connection myConn, PreparedStatement myPs){
		conn = myConn;
		ps=myPs;
		try{
			if(conn != null && ps != null){
				ps.close();
				conn.close();
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	/**
	 * 关闭conn、rs、ps三个参数，方法的重载
	 * @param myConn
	 * @param myPs
	 * @param myRs
	 * 创建者：TOM
	 * 创建日期：2013-8-6
	 */
	public void closeConn(Connection myConn, PreparedStatement myPs, ResultSet myRs){
		conn = myConn;
		ps=myPs;
		rs = myRs;
		try{
			if(conn != null && ps != null && rs != null){
				rs.close();
				ps.close();
				conn.close();
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
}
