package cn.edu.sjzc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ServerThread implements Runnable {
	Socket s = null;
	int clientId = 0;
	BufferedReader br = null;
	public static String winfail = "0";
	QiPu qp = new QiPu();
	DatabaseQiPu dp = new DatabaseQiPu();
	
	int x,y,p;

	public ServerThread(Socket s, int id) throws IOException {
		super();
		this.s = s;
		clientId = id;
		br = new BufferedReader(new InputStreamReader(s.getInputStream()));
	}

	public void run() {
		String clientInfo = "";
		try {
			// System.out.println("生成的随机数为："+Server.rm);

			Server.q1 = new ChessBoard(Server.rm);
			PrintStream ps = new PrintStream(Server.socketlist
					.get(clientId - 1).getOutputStream());
			ps.println(Server.rm);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			Server.a = Server.q1.initiate();

			while ((clientInfo = br.readLine()) != null) {
				if (Server.judgeClient % 2 == clientId - 1) {
					try{
						String[] strArray = clientInfo.split(",");
						x = Integer.parseInt(strArray[0]);
						y = Integer.parseInt(strArray[1]);
						p = Integer.parseInt(strArray[2]);
					}catch(Exception e){
						PrintStream ps = new PrintStream(Server.socketlist.get(
								clientId-1).getOutputStream());
						ps.println("-2,-2,-2,-2");
						continue;
					}
					if (p == 0) {
						if (x > 0 && y > 0 && x < Server.a.length
								&& y < Server.a[0].length) { // 若没越界，判断是否已有棋子
							if (Server.a[x][y] == "◆" || Server.a[x][y] == "◎") {
								System.out.println("此处已有棋子，请重新输入。。。");
								System.out.println("");
							} else {
								if (Server.count % 2 != 0) { // 甲方 count奇数 棋子◆
									Server.a[x][y] = "◆";
									System.out.println("甲方" + "输入的棋子横坐标：" + x);
									System.out.println("甲方" + "输入的棋子纵坐标：" + y);
									Server.count++;
									Server.judgeClient++;
									Date d = new Date();
									SimpleDateFormat sdf = new SimpleDateFormat(
											"yyyy-MM-dd HH:mm:ss");
									Server.tm1.put(Server.z1, x + "," + y);
									Server.tm3.put(Server.z1, sdf.format(d));
									Server.z1++;
									SuccessFail sf = new SuccessFail();
									boolean al = sf.success(Server.a, x, y);
									if (al == false) {
										winfail = "1";
										System.out.println("甲方胜出！");
										qp.printqipu();
										dp.saveGame();
										dp.saveDatabase();
									}
								} else {
									Server.a[x][y] = "◎";
									System.out.println("乙方" + "输入的棋子横坐标：" + x);
									System.out.println("乙方" + "输入的棋子纵坐标：" + y);
									Server.count++;
									Server.judgeClient++;
									Date d = new Date();
									SimpleDateFormat sdf = new SimpleDateFormat(
											"yyyy-MM-dd HH:mm:ss");
									Server.tm2.put(Server.z2, x + "," + y);
									Server.tm4.put(Server.z2, sdf.format(d));
									Server.z2++;
									SuccessFail sf1 = new SuccessFail();
									boolean al1 = sf1.success(Server.a, x, y);
									if (al1 == false) {
										winfail = "2";
										System.out.println("乙方胜出！");
										qp.printqipu();
										dp.saveGame();
										dp.saveDatabase();
									}

								}
							}
						}

					}
					if (x == 0 && y == 0) { // 重新开始
						winfail = "0";
						Server.count = 1;
						Server.a = Server.q1.initiate();
						Server.z1 = 1;
						Server.z2 = 1;
					}
					try{
						if (p == 2 && x != 0 && y != 0 && x != -1 && y != -1) { // 乙方悔棋
							winfail = "0";
							Server.count = 2 * y;
							Server.a = Server.q1.initiate();
							if (y == 1) {
								for (int u = 1; u <= y; u++) {
									String s1 = "";
									s1 = Server.tm1.get(u);
									String[] strArray2 = s1.split(",");
									int m2 = Integer.parseInt(strArray2[0]);
									int n2 = Integer.parseInt(strArray2[1]);
									Server.a[m2][n2] = "◆";
									Server.z1 = y + 1;
								}
								Server.z2 = y;
							} else {
								for (int u = 1; u <= y; u++) {
									String s1 = "";
									s1 = Server.tm1.get(u);
									String[] strArray2 = s1.split(",");
									int m2 = Integer.parseInt(strArray2[0]);
									int n2 = Integer.parseInt(strArray2[1]);
									Server.a[m2][n2] = "◆";
									Server.z1 = y + 1;
								}

								for (int u = 1; u < y; u++) {
									String s2 = "";
									s2 = Server.tm2.get(u);
									String[] strArray2 = s2.split(",");
									int m2 = Integer.parseInt(strArray2[0]);
									int n2 = Integer.parseInt(strArray2[1]);
									Server.a[m2][n2] = "◎";
									Server.z2 = y;
								}
							}
						}
						if (p == 1 && x != 0 && y != 0 && x != -1 && y != -1) { // 甲方悔棋
							winfail = "0";
							Server.count = 2 * y - 1;
							Server.a = Server.q1.initiate();
							if (y == 1) {
								Server.z1 = y;
								Server.z2 = y;
							} else {
								for (int u = 1; u < y; u++) {
									String s1 = "";
									s1 = Server.tm1.get(u);
									String[] strArray2 = s1.split(",");
									int m2 = Integer.parseInt(strArray2[0]);
									int n2 = Integer.parseInt(strArray2[1]);
									Server.a[m2][n2] = "◆";
									Server.z1 = y;
								}
								for (int u = 1; u < y; u++) {
									String s2 = "";
									s2 = Server.tm2.get(u);
									String[] strArray2 = s2.split(",");
									int m2 = Integer.parseInt(strArray2[0]);
									int n2 = Integer.parseInt(strArray2[1]);
									Server.a[m2][n2] = "◎";
									Server.z2 = y;
								}
							}
						}
					}catch(Exception e){
						PrintStream ps = new PrintStream(Server.socketlist.get(
								clientId-1).getOutputStream());
						ps.println("-3,-3,-3,-3");
						continue;						
					}
					for (int i = 0; i < Server.socketlist.size(); i++) {
						PrintStream ps = new PrintStream(Server.socketlist.get(
								i).getOutputStream());
						ps.println(clientInfo + "," + winfail);
					}
				} else {
					PrintStream ps = new PrintStream(Server.socketlist.get(
							clientId - 1).getOutputStream());
					ps.println("-1,-1,-1,-1");
				}
			}
		} catch (IOException ioe) {
			System.out.print(ioe);
		} catch (Exception e) {
			System.out.print(e);
			e.printStackTrace();
		}
	}
}
