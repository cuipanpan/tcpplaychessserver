package cn.edu.sjzc;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.TreeMap;
import java.lang.Thread;

public class Server {

	/**
	 * 服务器端
	 * 
	 * @param args
	 * cuipanpan 2013-7-30
	 */
	// public static int clientpd = 0;

	public static int judgeClient = 0;
	public static int jushu = 1; // 记录局数的
	// public static int judgeClientrm = 0;
	public static int z1 = 1, z2 = 1;
	static TreeMap<Integer, String> tm1 = new TreeMap<Integer, String>();
	static TreeMap<Integer, String> tm2 = new TreeMap<Integer, String>();
	static TreeMap<Integer, String> tm3 = new TreeMap<Integer, String>();
	static TreeMap<Integer, String> tm4 = new TreeMap<Integer, String>();
	public static ChessBoard q1 = null;
	public static String[][] a = null;
	static int rm = (int) (Math.random() * 10 + 6); // 棋盘大小为5-15

	public static ArrayList<Socket> socketlist = new ArrayList<Socket>();
	public static int count = 1;

	public static void main(String[] args) {
		try {
			int serverPort = 2345;
			int clientCount = 1;
			ServerSocket server = new ServerSocket(serverPort);
			while (true) {
				if (socketlist.size() < 2) {
					Socket clientSocket = server.accept();
					socketlist.add(clientSocket);
					new Thread(new ServerThread(clientSocket, clientCount))
							.start();

					clientCount++;
				} else {
					server.close();
				}
			}

		} catch (Exception e) {
			System.out.println("Listen：" + e.getMessage());
		}
	}

}
