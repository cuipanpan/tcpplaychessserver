package cn.edu.sjzc;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

public class QiPu {
	public void printqipu() {
		Date d = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		String filename = "d:/textserver/" + sdf.format(d) + ".txt";
		Iterator it1 = Server.tm1.keySet().iterator();
		Iterator it3 = Server.tm3.keySet().iterator();
		System.out.println("甲方");
		int h1 = 1;
		while (it1.hasNext()) {
			System.out.println("第" + h1 + "步：" + Server.tm1.get(it1.next())
					+ "下棋时间是：" + Server.tm3.get(it3.next()));
			try {
				FileOutputStream fos = new FileOutputStream(filename, true);
				String s1 = "";
				String s2 = "";
				s1 = Server.tm1.get(h1);
				s2 = Server.tm3.get(h1);
				String str1 = "甲方第" + h1 + "步：";
				byte[] buff2 = str1.getBytes();
				String str2 = "下棋时间是：";
				byte[] buff5 = str2.getBytes();
				String[] strArray1 = s1.split(",");
				byte[] buff = strArray1[0].getBytes();
				byte[] buff1 = strArray1[1].getBytes();
				String str3 = ",";
				byte[] buff3 = str3.getBytes();
				byte[] buff4 = s2.getBytes();
				fos.write(buff2);
				fos.write(buff);
				fos.write(buff3);
				fos.write(buff1);
				fos.write(buff5);
				fos.write(buff4);
				String str = "\r\n";
				byte[] buf = str.getBytes();
				fos.write(buf);
				fos.close();
			} catch (FileNotFoundException e1) {
				System.out.print(e1);
			} catch (IOException e2) {
				System.out.print(e2);
			}
			h1++;
		}

		Iterator it2 = Server.tm2.keySet().iterator();
		Iterator it4 = Server.tm4.keySet().iterator();
		System.out.println("乙方");
		int h2 = 1;
		while (it2.hasNext()) {
			System.out.println("第" + h2 + "步：" + Server.tm2.get(it2.next())
					+ "下棋时间是：" + Server.tm4.get(it4.next()));
			try {
				String s1 = "";
				String s2 = "";
				s1 = Server.tm2.get(h2);
				s2 = Server.tm4.get(h2);
				String[] strArray2 = s1.split(",");
				FileOutputStream fos = new FileOutputStream(filename, true);
				byte[] buff = strArray2[0].getBytes();
				byte[] buff1 = strArray2[1].getBytes();
				String str1 = "乙方第" + h2 + "步：";
				byte[] buff2 = str1.getBytes();
				String str2 = "下棋时间是：";
				byte[] buff5 = str2.getBytes();
				String str3 = ",";
				byte[] buff3 = str3.getBytes();
				byte[] buff4 = s2.getBytes();
				fos.write(buff2);
				fos.write(buff);
				fos.write(buff3);
				fos.write(buff1);
				fos.write(buff5);
				fos.write(buff4);
				String str = "\r\n";
				byte[] buf = str.getBytes();
				fos.write(buf);
				fos.close();
			} catch (FileNotFoundException e1) {
				System.out.print(e1);
			} catch (IOException e2) {
				System.out.print(e2);
			}
			h2++;
		}

	}
}
