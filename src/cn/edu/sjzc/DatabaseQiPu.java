package cn.edu.sjzc;

import java.sql.Date;
import java.util.Iterator;

import cn.edu.lecheng.dao.ChessDAO;

public class DatabaseQiPu {
	ChessDAO cd = new ChessDAO();

	public void saveDatabase() {
		Iterator it1 = Server.tm1.keySet().iterator();
		Iterator it3 = Server.tm3.keySet().iterator();
		int h1 = 1;
		while (it1.hasNext()) {
			boolean rut = cd.addChess("client 1", h1,
					Server.tm1.get(it1.next()), Server.tm3.get(it3.next()),
					Server.jushu);
			System.out.println("插入chees表" + rut);
			h1++;
		}
		Iterator it2 = Server.tm2.keySet().iterator();
		Iterator it4 = Server.tm4.keySet().iterator();
		int h2 = 1;
		while (it2.hasNext()) {
			boolean rut = cd.addChess("client 2", h2,
					Server.tm2.get(it2.next()), Server.tm4.get(it4.next()),
					Server.jushu);
			System.out.println("插入chees表" + rut);
			h2++;
		}
		Server.jushu++;
	}

	public void saveGame() {
		Iterator it3 = Server.tm3.keySet().iterator();
		Iterator it4 = Server.tm4.keySet().iterator();
		if (Server.count % 2 == 0) { // 甲方胜出
			boolean rut = cd.addGame(Server.jushu, Server.tm3.get(1),
					"client 1", Server.tm3.get(Server.count / 2));
			System.out.println("插入game表" + rut);
		} else { // 乙方胜出
			boolean rut = cd.addGame(Server.jushu, Server.tm3.get(1),
					"client 2", Server.tm4.get((Server.count - 1) / 2));
			System.out.println("插入game表" + rut);
		}
	}
}
